# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
import hashlib
import base64
from calculate.lib.utils.common import genpassword

_ = lambda x: x
from calculate.lib.cl_lang import (setLocalTranslate, getLazyLocalTranslate)

setLocalTranslate('cl_ldap3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)

ALL_BYTE_VALUES = "".join(chr(x) for x in xrange(256))


class SlapPasswd(object):
    """
    Получить хэш из пароля
    """

    def __init__(self, hashtype):
        self._hash = hashtype

    def _default(self, secret):
        pass

    def _generate_salt(self, salt_len=4, salt_chars=ALL_BYTE_VALUES):
        return genpassword(salt_len, salt_chars)

    def _sha1_base64(self, secret):
        return base64.b64encode(hashlib.sha1(secret).digest())

    def _salted_sha1_base64(self, secret):
        salt = self._generate_salt()
        return base64.b64encode("%s%s" % (
            hashlib.sha1("%s%s" % (secret, salt)).digest(), salt))

    def get_hash(self, secret):
        return "%s%s" % (
            self._hash,
            {
                '{SHA}': self._sha1_base64,
                '{SSHA}': self._salted_sha1_base64
            }.get(self._hash, self._default)(secret))

