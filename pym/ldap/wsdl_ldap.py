# -*- coding: utf-8 -*-

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys

from calculate.lib.datavars import VariableError, DataVarsError

from calculate.core.server.func import WsdlBase, CustomButton
from .ldap import Ldap, LdapError
from calculate.server.server import Server
from calculate.server.variables.action import Actions as ServerActions
from utils.cl_ldap_setup import ClLdapSetupAction

_ = lambda x: x
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate

setLocalTranslate('cl_ldap3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class Wsdl(WsdlBase):
    methods = [
        #
        # Настроить службу
        #
        {
            # идентификатор метода
            'method_name': Ldap.Method.Setup,
            # категория метода
            'category': __('Setup Server'),
            # заголовок метода
            'title': __("LDAP"),
            # иконка для графической консоли
            'image': 'calculate-ldap-setup,server-database,ooo-base,'
                     'office-database',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-ldap-setup',
            # права для запуска метода
            'rights': ['ldap'],
            # объект содержащий модули для действия
            'logic': {'Ldap': Ldap,
                      'Server': Server},
            # описание действия
            'action': ClLdapSetupAction,
            # объект переменных
            'datavars': "ldap",
            'native_error': (VariableError, DataVarsError, LdapError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': ServerActions.Setup,
                        'server.cl_server_name!': "ldap",
                        'cl_autoupdate_set': 'on'
                        #  'cl_dispatch_conf_default': "usenew"
                        },
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(
                    _("LDAP Server"),
                    expert=('ld_base_dn', 'cl_ldap_pw_generate_set',
                            'cl_ldap_remove_set',
                            'cl_verbose_set',),
                    custom_buttons=[
                        CustomButton.run_method(
                            Ldap.Method.Setup, "but0", _("Remove"),
                            CustomButton.Behavior.setvalue(
                                'cl_ldap_remove_set', 'on'),
                            lambda Get: Get('server.sr_ldap_set') == 'on'),
                        CustomButton.next_button("but1")
                    ]
                ),
            ],
            'brief': {'next': __("Run"),
                      'name': __("Setup LDAP"), }
        },
    ]
