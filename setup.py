#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# setup.py --- Setup script for calculate-ldap

# Copyright 2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

__app__ = "calculate-ldap"
__version__ = "3.5.0_alpha1"

import os
from glob import glob
from distutils.core import setup
from distutils.command import install_data as module_install_data
from distutils.util import change_root, convert_path

packages = [
    "calculate."+str('.'.join(root.split(os.sep)[1:]))
    for root, dirs, files in os.walk('pym/ldap')
    if '__init__.py' in files
]

print packages

setup(
    name=__app__,
    version=__version__,
    description="LDAP service for Calculate utilities",
    author="Calculate Ltd.",
    author_email="support@calculate.ru",
    url="http://calculate-linux.org",
    license="http://www.apache.org/licenses/LICENSE-2.0",
    package_dir = {'calculate.ldap': "pym/ldap",
                   'calculate.lib.format': 'pym/format'},
    packages = packages + ["calculate.lib.format"])
